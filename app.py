# -*- coding: utf-8 -*-
from flask import Flask, render_template, jsonify
import json
import requests

from functions import extract_keywords

import socket   

import sys

from urllib.request import urlopen



app = Flask(__name__)

### GET CLIENT's GEOLOCATION ###
## get his ip adress
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

 
host_name = socket.gethostname()    
IPAddress = socket.gethostbyname(host_name)    
eprint("Your Computer Name is:" + host_name)    
eprint("Your Computer IP Address is:" + IPAddress) 

## use his IP adress to find his location

url = 'https://ipinfo.io/json?token=ebf5841ac7d414'

response = urlopen(url)

data = json.load(response)
eprint(data)
country = data['country']
city = data['city']
eprint('VILLE : ' , city)
datas = {'country': str(country), 'city': str(city)}
eprint('DATAS : ' , data)



location= data['loc']
loclst=location.split(',')
lat = loclst[0]
lon = loclst[1]
print ('LAT : ', lat, 'LON : ', lon )




### WEATHER API ###

METEO_API_KEY = "96b8198e16a614cc652ca39a979ad1b8" # Remplacez cette ligne par votre clé OPENWEATHERMAP

if METEO_API_KEY is None:
    # URL de test :
    METEO_API_URL = "https://samples.openweathermap.org/data/2.5/forecast?lat=0&lon=0&appid=xxx"
else: 
    # URL avec clé :
    METEO_API_URL = "https://api.openweathermap.org/data/2.5/forecast?lat=" + lat + "&lon=" + lon + "&appid=" + METEO_API_KEY


@app.route("/")
def hello():
    datas=data
    return render_template("dashboard.html", data=datas)

@app.route('/api/meteo/')
def meteo():
    response = requests.get(METEO_API_URL)
    content = json.loads(response.content.decode('utf-8'))
    print(response)

    if response.status_code != 200:
        return jsonify({
            'status': 'error',
            'message': 'La requête à l\'API météo n\'a pas fonctionné. Voici le message renvoyé par l\'API : {}'.format(content['message'])
        }), 500

    data = [] # On initialise une liste vide
    for prev in content["list"]:
        datetime = prev['dt'] * 1000
        temperature = prev['main']['temp'] - 273.15 # Conversion de Kelvin en °c
        temperature = round(temperature, 2)
        data.append([datetime, temperature])
        
        return jsonify({
        'status': 'ok', 
        'data': data
        })



### NEWS API ###

NEWS_API_KEY = "fbcc6a99c8fe4a978832a56264afee47" # Remplacez None par votre clé NEWSAPI, par exemple "4116306b167e49x993017f089862d4xx"

if NEWS_API_KEY is None:
    # URL de test :
    NEWS_API_URL = "https://s3-eu-west-1.amazonaws.com/course.oc-static.com/courses/4525361/top-headlines.json" # exemple de JSON
else:
    # URL avec clé :
    NEWS_API_URL = "https://newsapi.org/v2/top-headlines?sortBy=publishedAt&pageSize=100&language=fr&apiKey=" + NEWS_API_KEY

@app.route('/api/news/')
def get_news():
 
    response = requests.get(NEWS_API_URL)

    content = json.loads(response.content.decode('utf-8'))

    if response.status_code != 200:
        return jsonify({
            'status': 'error',
            'message': 'La requête à l\'API des articles d\'actualité n\'a pas fonctionné. Voici le message renvoyé par l\'API : {}'.format(content['message'])
        }), 500

    keywords, articles = extract_keywords(content["articles"])

    return jsonify({
        'status'   : 'ok',
        'data'     :{
            'keywords' : keywords[:100], # On retourne uniquement les 100 premiers mots
            'articles' : articles
        }
    })


### OTHERS ###
    
if __name__ == "__main__":
    app.run(debug=True)
